#include "FadeInEffect.h"
#include <iostream>

FadeInEffect::FadeInEffect(Uint32 color) : m_FadeDuration(1000), m_CurrentTime(0), color(color) // Default fade duration: 1 second
{
}

FadeInEffect::~FadeInEffect()
{
}


void FadeInEffect::initializeEffect(SDL_Window* window, SDL_Surface* first, SDL_Surface* second)
{
    m_First = first;
    m_Second = second;
}

void FadeInEffect::updateEffect(SDL_Surface* surface, int deltaTime)
{

}


void FadeInEffect::drawEffect(SDL_Surface* surface, int deltaTime)
{

    SDL_Rect destRect = { 0, 0, surface->w, surface->h };
    
    m_CurrentTime = (m_CurrentTime + deltaTime) % m_FadeDuration;  // temporary, restarts at the end

    float progress = 1.0f - (static_cast<float>(m_CurrentTime) / m_FadeDuration);

    Uint8 alpha = static_cast<Uint8>(255 * progress);

    SDL_FillRect(m_Second, &destRect, color);

    SDL_SetSurfaceAlphaMod(surface, 256);
    SDL_SetSurfaceAlphaMod(m_Second, alpha);

    SDL_BlitSurface(m_Second, nullptr, surface, nullptr);



}
