#include "RotatingStars.h"
#include "Definitions.h"
#include "cmath"
#include <algorithm> 

void putpixel(SDL_Surface* surface, int x, int y, Uint32 pixel)
{
	// Clipping
	if ((x < 0) || (x >= SCREEN_WIDTH) || (y < 0) || (y >= SCREEN_HEIGHT))
		return;

	int bpp = surface->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to set */
	Uint8* p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;

	switch (bpp) {
	case 1:
		*p = pixel;
		break;

	case 2:
		*(Uint16*)p = pixel;
		break;

	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
			p[0] = (pixel >> 16) & 0xff;
			p[1] = (pixel >> 8) & 0xff;
			p[2] = pixel & 0xff;
		}
		else {
			p[0] = pixel & 0xff;
			p[1] = (pixel >> 8) & 0xff;
			p[2] = (pixel >> 16) & 0xff;
		}
		break;

	case 4:
		*(Uint32*)p = pixel;
		break;
	}
}

void renderStars(SDL_Surface* screenSurface, TStar* stars, int numStars) {
	// Update all stars
	for (int i = 0; i < numStars; i++) {
		// Calculate the effective size of the star based on its speed
		int starSize = static_cast<int>(stars[i].size * 3); // Scale factor 3 can be adjusted

		// Draw a circle centered around the star's position
		int starX = static_cast<int>(stars[i].x);
		int starY = static_cast<int>(stars[i].y);
		int radius = starSize / 2;

		// Draw the circle using midpoint circle algorithm
		int x = radius, y = 0;
		int radiusError = 1 - x;

		while (x >= y) {
			putpixel(screenSurface, starX + x, starY + y, stars[i].color);
			putpixel(screenSurface, starX - x, starY + y, stars[i].color);
			putpixel(screenSurface, starX + x, starY - y, stars[i].color);
			putpixel(screenSurface, starX - x, starY - y, stars[i].color);
			putpixel(screenSurface, starX + y, starY + x, stars[i].color);
			putpixel(screenSurface, starX - y, starY + x, stars[i].color);
			putpixel(screenSurface, starX + y, starY - x, stars[i].color);
			putpixel(screenSurface, starX - y, starY - x, stars[i].color);

			y++;

			if (radiusError < 0)
				radiusError += 2 * y + 1;
			else {
				x--;
				radiusError += 2 * (y - x + 1);
			}
		}
	}
}

void RotatingStars::initializeEffect()
{
	deltaTime = 0;
	// Allocate memory for all our stars
	stars = new TStar[MAXSTARS];
	// Randomly generate some stars
	for (int i = 0; i < MAXSTARS; i++)
	{
		stars[i].x = (float)(rand() % SCREEN_WIDTH);
		stars[i].y = (float)(rand() % SCREEN_HEIGHT);
		stars[i].size = 1.0f + static_cast<float>(rand() % 3); // Random size between 1 and 3

		// Assigning color randomly
		if (rand() % 100 < 30) // 30% chance for white color
			stars[i].color = 0xFFFFFFFF; // White
		else
		{
			// Randomly select between red and blue for the remaining stars
			if (rand() % 2 == 0)
				stars[i].color = 0xFFFF0000; // Red
			else
				stars[i].color = 0xFF0000FF; // Blue
		}
	}

	// Initialize the static stars
	staticStarsCount = MAXSTARS / 10; // 10% of total stars as static stars
	staticStars = new TStar[staticStarsCount];
	for (int i = 0; i < staticStarsCount; i++)
	{
		staticStars[i].x = (float)(rand() % SCREEN_WIDTH);
		staticStars[i].y = (float)(rand() % SCREEN_HEIGHT);
		staticStars[i].size = 0.1f; // Initial size for static stars
		staticStars[i].color = 0xFF800080; // Purple color
	}
}


void RotatingStars::updateEffect(SDL_Surface*)
{
	// Update all stars
	for (int i = 0; i < MAXSTARS; i++)
	{
		// Move this star to the right with speed based on size
		float starSpeed = 0.5f * stars[i].size; // Adjust the multiplier here for desired speed
		stars[i].x += starSpeed;

		// Additional sinusoidal movement for white stars
		if (stars[i].color == 0xFF0000FF) // Blue stars
		{
			float amplitude = 0.5f; // Adjust the amplitude of the pulsation
			float frequency = 0.1f; // Adjust the frequency of the pulsation
			stars[i].size = stars[i].size + amplitude * sin(frequency * stars[i].x);
		}

		// Additional sinusoidal movement for white stars
		if (stars[i].color == 0xFFFFFFFF) // White stars
		{
			float amplitude = 0.75f; // Adjust the amplitude of the sinusoidal movement
			float frequency = 0.5f; // Adjust the frequency of the sinusoidal movement
			stars[i].y += amplitude * sin(frequency * stars[i].x);
		}

		// Check if it's gone out of the right of the screen
		if (stars[i].x > SCREEN_WIDTH)
		{
			// If so, smoothly transition it to the left and reset size
			stars[i].x -= SCREEN_WIDTH;
			stars[i].size = 1.0f + static_cast<float>(rand() % 3); // Random size between 1 and 3
		}
	}

	// Update static stars
	for (int i = 0; i < staticStarsCount; i++)
	{
		// Increase size much faster
		staticStars[i].size += 1.0f;

		// Check if the star size exceeds the screen dimensions
		if (staticStars[i].size * 3 > std::max(SCREEN_WIDTH, SCREEN_HEIGHT))
		{
			// Reset size if it exceeds the screen dimensions
			staticStars[i].size = 0.1f;
		}
	}

	// Show only one or two static stars
	int visibleStars = 0;
	for (int i = 0; i < staticStarsCount; i++)
	{
		if (rand() % 100 < 5) // 5% chance to show each static star
		{
			staticStars[i].size = 0.1f; // Reset size
			visibleStars++;
			if (visibleStars >= 1) {
				break;
			}
		}
	}
}


void RotatingStars::drawEffect(SDL_Surface* surface, int newTime)
{
	deltaTime = newTime - lastTime;

	//Fill with black
	SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0, 0, 0));

	// Render dynamic stars
	renderStars(surface, stars, MAXSTARS);

	// Render static stars
	renderStars(surface, staticStars, staticStarsCount);
}