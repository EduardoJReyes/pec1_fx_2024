#include "SlideTransition.h"
#include <iostream>

SlideTransition::SlideTransition() {}

SlideTransition::~SlideTransition() {}

void SlideTransition::initializeEffect(SDL_Window* window, SDL_Surface* first, SDL_Surface* second) {
    m_First = first;
    m_Second = second;
}

void SlideTransition::initializeEffect()
{
}

void SlideTransition::updateEffect(SDL_Surface* surface, int deltaTime) {
    // Here, we can update the transition progress based on the deltaTime
    m_CurrentTime += deltaTime;
}

void SlideTransition::updateEffect(SDL_Surface* surface)
{
}

void SlideTransition::drawEffect(SDL_Surface* surface, int lastTime) {
        // Update the transition effect based on deltaTime
        // Here, we can update the transition progress based on the deltaTime
    if (m_CurrentTime == 0) {
        m_CurrentTime = lastTime; //used the first time, to sync the transition
    }
    int delta = (lastTime - m_CurrentTime) % m_TransitionTime; // temporary, restarts at the end

    // Calculate the position of the sliding transition based on the current time and transition duration
    float progress = static_cast<float>(delta) / m_TransitionTime;
    int yOffset = static_cast<int>(progress * surface->h);

    // Draw the first surface shifted downwards
    SDL_Rect destRect1 = { 0, yOffset, surface->w, surface->h - yOffset };
    SDL_BlitSurface(m_First, &destRect1, surface, &destRect1);

    // Draw the second surface above the first one
    SDL_Rect destRect2 = { 0, 0, surface->w, yOffset };
    SDL_BlitSurface(m_Second, &destRect2, surface, &destRect2);
}