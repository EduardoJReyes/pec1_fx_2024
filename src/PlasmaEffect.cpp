#include "PlasmaEffect.h"
#include "Definitions.h"
#include <cmath>
#include <iostream>

void PlasmaEffect::buildPalette() {
    for (int i = 0; i < 256; i++)
    {
        // Set the red component to vary between 0 and 255
        palette[i].R = i;

        // Set the green and blue components to zero
        palette[i].G = 0;
        palette[i].B = 0;
    }
}


void PlasmaEffect::initializeEffect()
{
    plasma1 = (unsigned char*)malloc(SCREEN_WIDTH * SCREEN_HEIGHT * 4);
    plasma2 = (unsigned char*)malloc(SCREEN_WIDTH * SCREEN_HEIGHT * 4);

    int i, j, dst = 0;
    for (j = 0; j < (SCREEN_HEIGHT * 2); j++)
    {
        for (i = 0; i < (SCREEN_WIDTH * 2); i++)
        {
            plasma1[dst] = (unsigned char)(128 + 127 * (sin((double)hypot(SCREEN_HEIGHT - j, SCREEN_WIDTH - i) / 64))); // Adjust scaling factor to make regions larger
            plasma2[dst] = (unsigned char)(128 + 127 * (sin((float)i / (37 + 15 * cos((float)j / 74))) * cos((float)j / (31 + 11 * sin((float)i / 57)))));
            dst++;
        }
    }
}


void PlasmaEffect::updateEffect(SDL_Surface* surface)
{
    // setup some nice colours, different every frame
    // this is a palette that wraps around itself, with different period sine
    // functions to prevent monotonous colours
    buildPalette();

    // Move plasma with more sine functions :)
    // Adjust the coefficients to slow down the movement even more
    Windowx1 = (SCREEN_WIDTH / 2) + (int)(((SCREEN_WIDTH / 2) - 1) * cos((double)currentTime / 4000));
    Windowx2 = (SCREEN_WIDTH / 2) + (int)(((SCREEN_WIDTH / 2) - 1) * sin((double)-currentTime / 4560));
    Windowy1 = (SCREEN_HEIGHT / 2) + (int)(((SCREEN_HEIGHT / 2) - 1) * sin((double)currentTime / 4920));
    Windowy2 = (SCREEN_HEIGHT / 2) + (int)(((SCREEN_HEIGHT / 2) - 1) * cos((double)-currentTime / 3000));

    // We only select the part of the precalculated buffer that we need
    src1 = Windowy1 * (SCREEN_WIDTH * 2) + Windowx1;
    src2 = Windowy2 * (SCREEN_WIDTH * 2) + Windowx2;
}

void PlasmaEffect::drawEffect(SDL_Surface* screenSurface, int lastTime) {
    currentTime = lastTime;

    if (screenSurface == nullptr || screenSurface->pixels == nullptr) {
        std::cerr << "Surface or pixel buffer is null!" << std::endl;
        return;
    }

    // Draw both halves of the plasma symmetrically
    for (int j = 0; j < SCREEN_HEIGHT; j++) {
        for (int i = 0; i < SCREEN_WIDTH / 2; i++) {
            int indexColor = (plasma1[src1] + plasma2[src2]) % 256;
            RGBColor color = palette[indexColor];

            // Calculate the mirrored pixel coordinates
            int mirroredI = SCREEN_WIDTH - i - 1;
            int mirroredSrc1 = (j * (SCREEN_WIDTH * 2)) + mirroredI;
            int mirroredSrc2 = mirroredSrc1;

            // Calculate the pixel addresses
            Uint32* pixelAddr = (Uint32*)((Uint8*)screenSurface->pixels + j * screenSurface->pitch + i * screenSurface->format->BytesPerPixel);
            Uint32* mirroredPixelAddr = (Uint32*)((Uint8*)screenSurface->pixels + j * screenSurface->pitch + mirroredI * screenSurface->format->BytesPerPixel);

            // Set the color for both pixels
            *pixelAddr = 0xFF000000 + (color.R << 16) + (color.G << 8) + color.B;
            *mirroredPixelAddr = *pixelAddr;

            src1++;
            src2++;
        }
        src1 += SCREEN_WIDTH;
        src2 += SCREEN_WIDTH;
    }
}

