#include "PerlinNoiseEffect.h"
#include <iostream>
#include <random>

PerlinNoiseEffect::PerlinNoiseEffect(int width, int height, float scale, int octaves, float persistence)
    : m_Width(width), m_Height(height), m_Scale(scale), m_Octaves(octaves), m_Persistence(persistence) {}

PerlinNoiseEffect::~PerlinNoiseEffect() {}

void PerlinNoiseEffect::initializeEffect() {
    // Initialize the noise map with random values
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0.0f, 1.0f);

    m_NoiseMap.resize(m_Height);
    for (int y = 0; y < m_Height; ++y) {
        m_NoiseMap[y].resize(m_Width);
        for (int x = 0; x < m_Width; ++x) {
            m_NoiseMap[y][x] = dis(gen);
        }
    }
}

void PerlinNoiseEffect::updateEffect(SDL_Surface* surface) {
    // Update the noise map with new random values
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0.0f, 1.0f);

    for (int y = 0; y < m_Height; ++y) {
        for (int x = 0; x < m_Width; ++x) {
            m_NoiseMap[y][x] = dis(gen);
        }
    }
}

void PerlinNoiseEffect::drawEffect(SDL_Surface* surface, int lastTime) {
    // Draw the noise map onto the surface
    if (surface == nullptr || surface->pixels == nullptr) {
        std::cerr << "Surface or pixel buffer is null!" << std::endl;
        return;
    }

    // Scale the noise values to the color range (0-255)
    Uint8 colorValue;
    SDL_LockSurface(surface);
    for (int y = 0; y < m_Height; ++y) {
        for (int x = 0; x < m_Width; ++x) {
            colorValue = static_cast<Uint8>(m_NoiseMap[y][x] * 255);
            Uint32* pixelAddr = (Uint32*)((Uint8*)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
            *pixelAddr = SDL_MapRGB(surface->format, colorValue, colorValue, colorValue);
        }
    }
    SDL_UnlockSurface(surface);
}
