#include "CrossFadeEffect.h"
#include <iostream>

CrossFade::CrossFade() : m_FadeDuration(1000), m_CurrentTime(0) // Default fade duration: 1 second
{
}

CrossFade::~CrossFade()
{
}


void CrossFade::initializeEffect(SDL_Window* window, SDL_Surface* first, SDL_Surface* second)
{
    m_First = first;
    m_Second = second;
}

void CrossFade::updateEffect(SDL_Surface* surface, int deltaTime)
{

}


void CrossFade::drawEffect(SDL_Surface* surface, int deltaTime)
{

    SDL_Rect destRect = { 0, 0, surface->w, surface->h };
   
    m_CurrentTime = (m_CurrentTime + deltaTime) % m_FadeDuration;  

    float progress = static_cast<float>(m_CurrentTime) / m_FadeDuration;

    Uint8 alpha = static_cast<Uint8>(255 * progress);

    SDL_SetSurfaceAlphaMod(surface, 256);
    SDL_SetSurfaceAlphaMod(m_Second, alpha);

    SDL_BlitSurface(m_Second, nullptr, surface, nullptr);



}
