#include "SquareEffect.h"
#include "Definitions.h"
#include "SDL.h" 
#include <iostream>
#include <vector>
#include <cmath>
#include "SDL_mixer.h" 
#include "SDL_audio.h"
#include "vector.h"
#include <chrono> 
#include <thread> 

float growthFactor = 1.0f; // Growth pace for SQUARE EFFECT
float spawnInterval = 0.1f; // Time interval between spawns in seconds
const int LINE_THICKNESS = 50; //Line thickness for the SQUARE EFFECT


void drawPixel(SDL_Surface* surface, int x, int y, Uint32 pixel) {
	// Ensure the coordinates are within the surface bounds
	if (x >= 0 && x < surface->w && y >= 0 && y < surface->h) {
		// Calculate the pixel offset based on the screen width and the position
		int offset = y * surface->pitch + x * sizeof(Uint32);

		// Set the pixel value
		*reinterpret_cast<Uint32*>(reinterpret_cast<Uint8*>(surface->pixels) + offset) = pixel;
	}
}

void drawThickSquare(SDL_Surface* surface, int x, int y, int size, Uint32 pixel) {
	// Draw the square outline with increased thickness
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < LINE_THICKNESS; ++j) {
			drawPixel(surface, x + i, y - j, pixel); // Top line
			drawPixel(surface, x + i, y + size - 1 + j, pixel); // Bottom line
			drawPixel(surface, x - j, y + i, pixel); // Left line
			drawPixel(surface, x + size - 1 + j, y + i, pixel); // Right line
		}
	}

	// Fill the corners to connect the lines
	for (int i = 0; i < LINE_THICKNESS; ++i) {
		for (int j = 0; j < LINE_THICKNESS; ++j) {
			drawPixel(surface, x - i, y - j, pixel); // Top-left corner
			drawPixel(surface, x + size - 1 + i, y - j, pixel); // Top-right corner
			drawPixel(surface, x - i, y + size - 1 + j, pixel); // Bottom-left corner
			drawPixel(surface, x + size - 1 + i, y + size - 1 + j, pixel); // Bottom-right corner
		}
	}
}

float distanceFromCenter(int x, int y) {
	int centerX = SCREEN_WIDTH / 2;
	int centerY = SCREEN_HEIGHT / 2;
	return std::sqrt(std::pow(x - centerX, 2) + std::pow(y - centerY, 2));
}

Uint32 colorFromDistance(float distance, int lastTime, int squareSize, SDL_Surface* screenSurface) {
	// Calculate intensity inversely proportional to square size
	int intensity = static_cast<int>((1.0 - squareSize / static_cast<float>(SCREEN_WIDTH)) * 0xFF);
	intensity = std::max(intensity, 0); // Ensure intensity is not negative

	// Interpolate between white and black based on square size
	Uint8 r = static_cast<Uint8>((static_cast<float>(squareSize) / SCREEN_WIDTH) * 0xFF);
	Uint8 g = static_cast<Uint8>((static_cast<float>(squareSize) / SCREEN_WIDTH) * 0xFF);
	Uint8 b = static_cast<Uint8>((static_cast<float>(squareSize) / SCREEN_WIDTH) * 0xFF);

	return SDL_MapRGB(screenSurface->format, r, g, b);
}

void drawMySquares(std::vector<int>& squares, SDL_Surface* screenSurface, int lastTime) {
    // Update existing squares and draw them
    for (int i = 0; i < squares.size(); ++i) {
        int x = SCREEN_WIDTH / 2 - squares[i] / 2;
        int y = SCREEN_HEIGHT / 2 - squares[i] / 2;

        Uint32 color = colorFromDistance(distanceFromCenter(x, y), lastTime, squares[i], screenSurface);
        drawThickSquare(screenSurface, x, y, squares[i], color);

        squares[i] += static_cast<int>(growthFactor);

        if (squares[i] > SCREEN_WIDTH) {
            squares.erase(squares.begin() + i);
            --i;
        }
    }
}

void SquareEffect::initializeEffect()
{
	// Initialize squares covering the entire screen
	squares.push_back(std::max(SCREEN_WIDTH, SCREEN_HEIGHT)); // Initial square size covering the largest dimension
}

void SquareEffect::updateEffect(SDL_Surface* surface)
{
	// Update spawn timer
	spawnTimer += msFrame / 1000.0f;
	// Add a new square if the spawn interval has passed
	if (spawnTimer >= spawnInterval) {
		squares.push_back(1);
		spawnTimer = 0.0f; // Reset the spawn timer
	}
}

void SquareEffect::drawEffect(SDL_Surface* surface, int lastTime)
{
	drawMySquares(squares, surface, lastTime);


}
