#include "Definitions.h"
#include "SDL.h" 
#include <iostream>
#include <vector>
#include <cmath>
#include "SDL_mixer.h" 
#include "SDL_audio.h"
#include "vector.h"
#include "matrix.h"
#include "Effect.h"
#include "SquareEffect.h"
#include "RotatingStars.h"
#include "PlasmaEffect.h"
#include "SlideTransition.h"
#include "PerlinNoiseEffect.h"
#include "Transition.h"
#include "FadeOutEffect.h"
#include "FadeInEffect.h"
#include "CrossFadeEffect.h"

// Global variables
Mix_Music* mySong;
SDL_Window* window = NULL; 
SDL_Surface* screenSurface = NULL; 

int lastTime = 0, currentTime, deltaTime;

// Function declarations
void initializeSDL();
void closeResources();
void initMusic();

int main(int argc, char* args[]) {
    initializeSDL();
    initMusic();

    screenSurface = SDL_GetWindowSurface(window);
    SDL_SetSurfaceBlendMode(screenSurface, SDL_BLENDMODE_BLEND);
    
    bool quit = false;
    
    SDL_Event e;
    SquareEffect Squares{};
    PlasmaEffect Plasma{};
    RotatingStars Stars{};
    PerlinNoiseEffect Noise(SCREEN_WIDTH, SCREEN_HEIGHT, 0.1f, 8, 0.5f);
    SlideTransition Slide{};

    FadeInEffect FadeInRed{0XFF0000};

    FadeOutEffect FadeOut{0X0};
    FadeOutEffect FadeOut2{0X0};
    FadeOutEffect FadeOut3{0X0};
   

    FadeOutEffect FadeOutRed{0XFF0000};
    FadeOutEffect FadeOutRed2{0XFF0000};

    CrossFade CrossFade{};

    Effect* activeEffect{};
    Effect* secondaryEffect{};
    Effect* transition{};
    Transition* fade{};
 

    Squares.initializeEffect();
    Plasma.initializeEffect();
    Stars.initializeEffect();
    Noise.initializeEffect();

    SDL_Surface* secondarySurface = SDL_CreateRGBSurface(screenSurface->flags, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0, 0, 0, 0);

    SDL_SetSurfaceBlendMode(secondarySurface, SDL_BLENDMODE_BLEND);
    
    Slide.initializeEffect(window, screenSurface, secondarySurface);
    CrossFade.initializeEffect(window, screenSurface, secondarySurface);
    FadeInRed.initializeEffect(window, screenSurface, secondarySurface);

    FadeOut.initializeEffect(window, screenSurface, secondarySurface);
    FadeOut2.initializeEffect(window, screenSurface, secondarySurface);
    FadeOut3.initializeEffect(window, screenSurface, secondarySurface);

    FadeOutRed.initializeEffect(window, screenSurface, secondarySurface);
    FadeOutRed2.initializeEffect(window, screenSurface, secondarySurface);

    while (!quit && lastTime < 22000) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_FillRect(screenSurface, NULL, 0);

        int cycleTime = lastTime; 

        if (cycleTime < 3000) {
            //We start with just noise
            activeEffect = &Noise;
            fade = nullptr;
            secondaryEffect = nullptr;
            transition = nullptr;
        }

        
        else if (cycleTime < 4000) {
            // The noise fades out to the Squares
            activeEffect = &Noise;
            fade = &FadeOut;
            secondaryEffect = &Squares;
            transition = nullptr;
        }

        else if (cycleTime < 7000) {
            // Just the squares now
            activeEffect = &Squares;
            secondaryEffect = nullptr;
            transition = nullptr;
            fade = nullptr;
        }

        else if (cycleTime < 8000) {
            // The squares fade to red
            activeEffect = &Squares;
            secondaryEffect = nullptr;
            transition = nullptr;
            fade = &FadeOutRed;
        }

        else if (cycleTime < 9000) {
            // And from red we fade to the plasma
            activeEffect = &Plasma;
            secondaryEffect = nullptr;
            transition = nullptr;
            fade = &FadeInRed;
        }

        else if (cycleTime < 10000) {
            // Now it's just plasma for a while
            activeEffect = &Plasma;
            secondaryEffect = nullptr;
            transition = nullptr;
            fade = nullptr;
        }

        else if (cycleTime < 11000) {
            // CrossFade, plasma to Squares again
            activeEffect = &Plasma;
            secondaryEffect = &Squares;
            transition = nullptr;
            fade = &CrossFade;
        }

        else if (cycleTime < 14000) { 
            // Squares again now
            activeEffect = &Squares;
            secondaryEffect = nullptr;
            transition = nullptr;
            fade = nullptr;
        }

        else if (cycleTime < 15000) { 
            // Squares fade to Stars
            activeEffect = &Squares;
            secondaryEffect = &Stars;
            transition = nullptr;
            fade = &FadeOut2;
        }

        else if (cycleTime < 18000) {
            // Now it's just Stars
            SDL_SetSurfaceAlphaMod(screenSurface, 255);
            SDL_SetSurfaceAlphaMod(secondarySurface, 255);
            activeEffect = &Stars;
            secondaryEffect = nullptr;
            transition = nullptr;
            fade = nullptr;

        }

        else if (cycleTime < 21000) {
            // Now the noise slides into the stars
            activeEffect = &Stars;
            secondaryEffect = &Noise;
            transition = &Slide;
            fade = nullptr;

        }

        else{

            //Final fade to black
            activeEffect = &Noise;
            secondaryEffect = nullptr;
            transition = nullptr;
            fade = &FadeOut3;

        }

        // Update and draw activeEffect
        if (activeEffect) {
            activeEffect->updateEffect(screenSurface);
            activeEffect->drawEffect(screenSurface, lastTime);
        }

        // Update and draw secondaryEffect
        if (secondaryEffect) {
            secondaryEffect->updateEffect(secondarySurface);
            secondaryEffect->drawEffect(secondarySurface, lastTime);
        }

        // Apply transition
        if (transition) {
            transition->updateEffect(nullptr); // currently unused, it gets surfaces at initialization
            transition->drawEffect(screenSurface, lastTime); // sends time, but effect expects delta
        }

        // Apply fade effect
        if (fade) {

            fade->updateEffect(screenSurface, deltaTime);
            fade->drawEffect(screenSurface, deltaTime);
        }

        // Update window surface
        SDL_UpdateWindowSurface(window);

        // Cap the frame rate
        currentTime = SDL_GetTicks();
        deltaTime = currentTime - lastTime;
        if (deltaTime < msFrame) {
            SDL_Delay(static_cast<Uint32>(msFrame - deltaTime));
        }

        lastTime = SDL_GetTicks();
    }

    closeResources();

    return 0;
}

// Function definitions

void initializeSDL() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        exit(1);
    }

    window = SDL_CreateWindow("Expanding Squares", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        exit(2);
    }
}

void closeResources() {
    Mix_HaltMusic();
    Mix_FreeMusic(mySong);
    Mix_Quit();
    Mix_CloseAudio();
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void initMusic() {
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 4096);
    Mix_Init(MIX_INIT_OGG);
    mySong = Mix_LoadMUS("GeaffelsteinPursuitEdit.ogg");
    if (!mySong) {
        std::cout << "Error loading Music: " << Mix_GetError() << std::endl;
        closeResources();
        exit(1);
    }
    Mix_PlayMusic(mySong, 0);
    Mix_VolumeMusic(5); // 5, so the audio is not that high
}

