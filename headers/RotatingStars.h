#pragma once
#include "Effect.h"

#define MAXSTARS 256

struct TStar
{
    float x, y;             
    float size;             
    unsigned char plane;    
    unsigned int color;     
};

class RotatingStars : public Effect
{
private:
    TStar* stars;
    float deltaTime;
    float lastTime;
    int staticStarsCount;   
    TStar* staticStars;     

public:
    void initializeEffect() override;
    void updateEffect(SDL_Surface*) override;
    void drawEffect(SDL_Surface*, int) override;
};
