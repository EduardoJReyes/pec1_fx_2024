#pragma once
#include "Effect.h"
#include <vector>

class PerlinNoiseEffect : public Effect {
public:
    PerlinNoiseEffect(int width, int height, float scale, int octaves, float persistence);
    ~PerlinNoiseEffect();

    void initializeEffect() override;
    void updateEffect(SDL_Surface* surface) override;
    void drawEffect(SDL_Surface* surface, int lastTime) override;

private:
    int m_Width;
    int m_Height;
    float m_Scale;
    int m_Octaves;
    float m_Persistence;

    std::vector<std::vector<float>> m_NoiseMap;

    float noise(int x, int y);
    float smoothNoise(float x, float y);
    float interpolate(float a, float b, float x);
    float perlinNoise(float x, float y);
};


