#pragma once
#include "SDL.h" 

class Effect
{
public: 
	virtual~Effect() = default;
	virtual void initializeEffect() = 0;
	virtual void updateEffect(SDL_Surface*) = 0;
	virtual void drawEffect(SDL_Surface*, int) = 0;

protected:
	Effect() = default;
};

