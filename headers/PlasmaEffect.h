#pragma once
#include "Effect.h"

struct RGBColor { unsigned char R, G, B; };

class PlasmaEffect :
    public Effect
{
    unsigned char* plasma1;
    unsigned char* plasma2;
    int Windowx1, Windowy1, Windowx2, Windowy2;
    long src1, src2;
    RGBColor palette[256];
    void buildPalette();
    int currentTime;
    
public: 

    void initializeEffect() override;
    void updateEffect(SDL_Surface*) override;
    void drawEffect(SDL_Surface*, int) override;
};

