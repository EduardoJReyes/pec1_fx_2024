#pragma once
#include "Effect.h"
#include <vector>

class SquareEffect :
    public Effect
{
    std::vector<int> squares;
    float spawnTimer = 0.0f; // Timer to track time since the last square spawned
    

public:
    void initializeEffect() override;
    void updateEffect(SDL_Surface*) override;
    void drawEffect(SDL_Surface*, int) override;
    SquareEffect():Effect{}{}
    ~SquareEffect() override{}
};

