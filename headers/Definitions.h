#pragma once
#define FPS 60
constexpr float msFrame = 1 / (FPS / 1000.0f);
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
