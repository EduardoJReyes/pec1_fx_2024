#pragma once
#include "Transition.h"

class FadeInEffect : public Transition
{
public:
    FadeInEffect(Uint32 color);
    ~FadeInEffect();

    void initializeEffect(SDL_Window* window, SDL_Surface* first, SDL_Surface* second) override;
    void updateEffect(SDL_Surface* surface, int deltaTime) override; 
    void drawEffect(SDL_Surface* surface, int lastTime) override; 

private:
    Uint32 color;
    int m_FadeDuration = 1000; 
    int m_CurrentTime; 
};
