#pragma once
#include "Effect.h"

class Transition 
{
public:
    virtual~Transition() = default;
    
    virtual void initializeEffect(SDL_Window* window, SDL_Surface* first, SDL_Surface* second) = 0;
    virtual void updateEffect(SDL_Surface* surface, int deltaTime) = 0;
    virtual void drawEffect(SDL_Surface* surface, int deltaTime) = 0;
 
protected:
    Transition() = default;
    Uint8 m_Red{ 255 }; // Red component for the transition
    Uint8 m_Green{ 0 }; // Green component for the transition
    Uint8 m_Blue{ 0 };  // Blue component for the transition

    SDL_Surface* m_Surface{ nullptr };
    SDL_Surface* m_First{ nullptr };
    SDL_Surface* m_Second{ nullptr };
};
