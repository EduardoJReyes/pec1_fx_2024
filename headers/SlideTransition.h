#pragma once
#include "Effect.h"

class SlideTransition : public Effect
{
public:
    SlideTransition();
    ~SlideTransition();

    void initializeEffect(SDL_Window* window, SDL_Surface* first, SDL_Surface* second);
    void initializeEffect() override;
    void updateEffect(SDL_Surface* surface, int deltaTime);
    void updateEffect(SDL_Surface* surface) override;
    void drawEffect(SDL_Surface* surface, int lastTime) override;

private:
    SDL_Surface* m_First{ nullptr };
    SDL_Surface* m_Second{ nullptr };
    int m_TransitionTime{ 3000 }; 
    int m_CurrentTime{ 0 }; 
};
